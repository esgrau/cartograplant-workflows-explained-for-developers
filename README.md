# CartograPlant workflows explained for developers

## 1. Marker quality filtering

Althought CartograPlant contains other type of markers (nSSR, cpSSR and indels) apart from SNPs, SNPs are the most commonly used and versatile nowadays in high throughput sequencing so we are going to focus on SNPs for this workflow. **SNP quality control filtering** is a key step previous to further data analysis, such as Genome-Wide Association Analysis (GWAS), since it removes markers/individuals likely containing genotyping errors, ensuring the accuracy of the results. A standard SNP quality filtering for GWAS usually consists on the following steps.


- **Missigness per individual:** Removing individuals having more than a given percentage of missing data.

- **Minor allele count per marker:** Removing SNPs with a minor allele count lower than a given value.

- **Minimum quality score:** Removing SNPs with a quality score lower than a given value.

- **Minimum reads per marker (Depth):** Removing SNPs with less than a given number of reads.

- **Missigness per marker:** Removing SNPs with more than a given percentage of missing individuals per genotype.

- **Minor allele frequency:** Removing SNPs with a minor allele frequency lower than a given value.

- **Mendelian errors (for family-based data only):** Discarding families with more than a given frequency of Mendel errors (considering all SNPs) and also excluding SNPs with more than a given frequency of Mendelian error rate.

However, deciding the most suitable quality control filtering thresholds for your dataset can be tricky, since they depend on multiple factors. On the one hand, an excesively lax quality filtering threshold can reduce the quality of the SNP dataset, and thus, the accuracy of the results, as well as innecessarily increase the computational time. On the other hand, a too strict threshold can lead to a loss of important information, since it can lead to the removal of good quality SNP data.

**Imputation** of lacking SNPs is another important step in GWAS. It consists on the inference of the lacking SNPs on the dataset to increase the association power. The imputed genotypes expand the set of SNPs that can be tested for association, and this more comprehensive view of the genetic variation in a study can enhance true association signals and facilitate meta-analysis.

The selection of suitable quality filtering thresholds is also beneficial for this imputation step, since imputation methods most often make use only of genotypes that are successfully inferred after having passed these quality filtering threshold.
Most existing genotype imputation methods use patterns from known genotypes to impute missing genotypes. For model species, this imputation step is usually performed by combining a reference panel of individuals genotyped at a dense set of polymorphic sites (usually single-nucleotide polymorphisms, or “SNPs”) with a study sample collected from a genetically similar population and genotyped at a subset of these sites. This imputation method predicts unobserved genotypes in the study sample by using a population genetic model to extrapolate allelic correlations measured in the reference panel. To this end, markers from both reference and study panel must be ordered or phased. Consequently, this imputation method can be tricky to implement in non-model species, since requires high-quality reference genomes.

**LinkImputeR** (Money et al. 2017) is a program that tests for different filter parameters to perform data quality filtering, in order to maximize the quantity and quality of the resulting SNPs, while maintaining accuracy/correlation values. As a result, LinkImputeR provides a series of combinations of thresholds or "Cases", so that users can decide on thresholds that are most suitable for their purposes. As a consequence, it improves the power of downstream analysis. Once the best SNP quality filtering threshold (aka Case) is selected, LinkImpute uses it to perform imputation of lacking SNPs. This imputation step is specifically designed for non-model organisms since it requires neither ordered markers nor a reference panel of genotypes.
To this end, LinkImputeR uses an imputation method called LD-kNN Imputation, which is based on looking for SNPs in Linkage Disequilibrium (LD) with the lacking SNP to be imputed. Thus, to impute a genotype at SNP a in sample b, LD-kNNi first uses the l SNPs most in LD with the SNP to be imputed in order to calculate a distance from sample b to every other sample for SNP a. The algorithm proceeds by picking the k nearest neighbours to b that have an inferred genotype at SNP a and then scoring each of the possible genotypes, c g , as a weighted count of these genotypes.

The data quality filters tested and implemented in LinkImputerR are depth, minor allele frequency, and missingness by both SNP and sample. Thus, the rest of the quality filters (minor allele count per marker and minimum quality score) have to be performed before running LinkImputeR.

### 1.1. CartograPlant analysis visuals

The SNP quality filtering starts with the interactive graphs offered by CartograPlant in the *CartograPlant analysis* window. Here users can select the SNPs they want to retain for data analysis (if any, because they can also decide not to use any SNP and only retain phenotypes and environmental variables) and also the individuals, the phenotypes and the environmental variables. Throughout all the tabs, it is needed to add a window in one of the corners of the screen (when it fits better) showing the current selected number of individuals, markers, phenotypes and environmental data for further analysis to help users track the data the have selected throughout all the data selection steps. In order to help this decision (Individuals, genotype, phenotype and environmental data selection for further data analysis) some interactive graphs and visuals (histograms, heatmaps, PCAs) are provided. 

#### 1.1.1 Filter by trait data tab
![Window_traits3](Window_traits3.png "Window_traits3")
![Window_traits4](Window_traits4.png "Window_traits4")

### R code for the Phenotype data PCA

In this example, the phenotypes for the TGDR477 study are used
```
library(tidyverse)

#We read the table containing the phenotypes for the study TGDR477
TGDR477<-read.csv("TGDR477_Phenotype_Data_Populus_trichocarpa_NoNA.csv")
head(TGDR477[,1:10])
rownames(TGDR477)<- TGDR477[,1]
TGDR477_2<-TGDR477[,-1]
TGDR477_3<-TGDR477_2[,-1]#to eliminate the "ramets" variable (this part of the code is specific for this dataset)

#Select the variables of interest (for instance, here we are going to select the first 10 variables)
TGDR477_subset<-TGDR477_3[,c(1,2,3,4,5,6,7,8,9,10)]

#Remove the NAs
TGDR477_noNA<-na.omit(TGDR477_subset)

#Code to obtain the principal components (PCs)
TGDR477.pca <- prcomp(TGDR477_noNA, center = TRUE, scale. = TRUE)
summary(TGDR477.pca)
str(TGDR477.pca)

library(devtools)
#install_github("vqv/ggbiplot")
library(ggbiplot)

#Plot the PCA
ggbiplot(TGDR477.pca)
```

#### 1.1.2 Filter by genotype data tab

Here users select the SNPs they want to retain for further data analysis. To do so, I envision this interactive graph:

![Marker_selection_tab_1](Marker_selection_tab_1.png "Marker_selection_tab_1")
![Marker_selection_tab_2](Marker_selection_tab_2.png "Marker_selection_tab_2")
![Window_genotypes](Window_genotypes.png "Window_genotypes")

#### 1.1.3 Marker quality filtering by SNP and individual tabs 

These interactive histograms and PCAs will allow users to decide the best quality filtering thresholds for their selected markers and also, detect outliers in the marker dataset. These thresholds will be implemented (and the outliers removed) during the further *SNP quality filtering workflow* in the *Run analysis* tab. Users can select several thresholds to be tested using the "SNP quality filtering and Imputation: LinkImputeR" workflow in the Run Analysis tab, or just one threshold per quality variable to be run manually in the "Step by Step SNP quality filtering" workflow. Interactive histograms for the following quality variables are needed:

- **Missigness per individual:** Removing individuals having more than a given percentage of missing data.

- **Minor allele count per marker:** Removing SNPs with a minor allele count lower than a given value.

- **Minimum quality score:** Removing SNPs with a quality score lower than a given value.

- **Minimum reads per marker (Depth):** Removing SNPs with less than a given number of reads.

- **Missigness per marker:** Removing SNPs with more than a given percentage of missing individuals per genotype.

- **Minor allele frequency:** Removing SNPs with a minor allele frequency lower than a given value.

- **Mendelian errors (for family-based data only):** Discarding families with more than a given frequency of Mendel errors (considering all SNPs) and also excluding SNPs with more than a given frequency of Mendelian error rate.

Warning messages will be displayed when the interactive histograms detect a threshold that is not suitable for the SNP dataset, for instance, when 0 SNPs remain after implementing a given threshold (I still need to think about this idea a bit more, in which situations this warning messages will appear). The display will be just as a sentence in red, next to the histogram saying for instance: " Warning: 0 remaining SNP after implementing this threshold"

![Window_SNPs](Window_SNPs.png "Window_SNPs")
![Window_Indiv](Window_Indiv.png "Window_Indiv")
![Window_Indiv2](Window_Indiv2.png "Window_Indiv2")

#### 1.1.4 Environmental layer selection tab
![Window_env2](Window_env2.png "Window_env2")

#### 1.1.5 Run analysis tab
##### 1.1.5.1. SNP quality filtering

Two workflows will be available for SNP quality filtering. 

- SNP Quality filtering: Step by step: This workflow allows users to perform the SNP quality filtering following all the previously described variables using the thresholds of their choice, decided thanks to the interactive histograms
- SNP Quality filtering (threshold testing) + Imputation: LinkImputeR: This workflow has to steps. The first step (Accuracy mode) test for different combinations of thresholds, decided thanks to the interactive histograms, to test which combination of thresholds maximizes the number of SNPs while maintaining accuracy. After performing this testing, the following output is obtained with information to help decide users the best combination of thresholds (Cases) for their dataset (which one maximizes the number of SNPs and individuals while maintaining accuracy). Maybe we can plot the information of this output containing the tested Cases, but I am still figuring out how (and it is not essential, with the table for the moment is enough). Once the user select the Case of interest,they can put it as an input of the second step (Imputation), which applies the thresholds of the selected Case to quality filter the SNPs and also perform imputation of the lacking SNPs

Table containing the Cases information (output of LinkImputeR)

![Cases](Cases.png "Cases")

A detailed description about what each of the workflows do will be displayed once the users select the workflow, as it is shown in the image bellow: 

![Imputation](Imputation.png "Imputation")

##### 1.1.5.2. Population structure

##### DAPC

This is the code for DAPC analysis with guidelines for the users and with notes for the developers to know what to expect after running each step

````
#These are the R packages needed to run DAPC
library(adegenet)
library(vcfR)

#The only input file needed is a vcf or vcf.gz file
vcf <- read.vcfR("Panel1imputedLD0.2.vcf")
x <- vcfR2genlight(vcf) #transform vcf to genlight

#After running this step, a plot and a question is generated. This plot has to be provided to the users to help them decide the number of PCs to retain

grp <- find.clusters(x, max.n.clust=40)
Number of PCs to retain:

# For users: This function displays a graph of cumulated variance (Y axis) explained by the eigenvalues of the PCA (X axis). Apart from computational time, there is no reason for keeping a small number of components.Then, we aim to keep all the information. For instance, if the number of PCs in your plot (X axis) is 110, you should retain 200 PCs so all of them are kept

#Then, a second question and another plot. The function displays a graph of BIC values for increasing values of k. If the graph shows, for instance, a clear decrease of BIC until k = 6 clusters, after which BIC increases, in this case, the elbow in the curve also matches the smallest BIC, so 6 clusters should be retained. In practice, the choice is often trickier to make for empirical dataset.
Number of clusters to retain:

# Here, the DAPC is calculated. This step displays the same graph of cumulated variance as in find.cluster. However,unlike k-means, DAPC can benefit from not using too many PCs. Indeed, retaining too many components with respect to the number of individuals can lead to over-fitting and unstability in the membership probabilities returned by the method. DAPC is implemented by the function "dapc", which first transforms the data using PCA, and then performs a Discriminant Analysis on the retained principal components.Two graphs will appear as a result to help decide the number of principal components and the number of discriminant functions to be retained. The bottomline is therefore retaining a few PCs without sacrificing too much information. Thus, the best number of PCs to retain is the value from which the curve starts to get flatter. Once the number of PCs to retain are selected, a barplot of eigenvalues for the discriminant analysis is displayed, asking for a number of discriminant functions to retain. For small number of clusters, all eigenvalues can be retained since all discriminant functions can be examined without difficulty. Whenever more (say, tens of) clusters are analysed, it is likely that the first few dimensions will carry more information than the others, and only those can then be retained and interpreted.
dapc1 <- dapc(x, grp$grp)
Number of PCs to retain:
Number of clusters to retain:

#Plot: Select the colors for the populations in the plot (it has to be the same number of colors as the number of clusters retained in the previous step)
myCol=c("green","blue","yellow","red","orange")
scatter(dapc1, posi.da="bottomleft", bg="white",
        pch=17:22, cstar=0, col=myCol, scree.pca=TRUE,
        posi.pca="bottomright")


#Membership probabilities

#Change formatting for storage and used to correct by population structure in the GWAS with EMMAX
round<-round(dapc1$posterior,3)
assignedPop<-apply(round, 1, which.max)
assignedPopframe<-as.data.frame(assignedPop)

#Including intercept
Intercept<-rep(1,times=407)
assignedPopInter<-cbind(Intercept,assignedPopframe)

#Print table
write.table(assignedPopInter,"Panel1assignedPopDAPC.txt", col.names=F,quote=F,sep=" ")
````








